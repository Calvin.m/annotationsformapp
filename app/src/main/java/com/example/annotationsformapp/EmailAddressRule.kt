package com.example.annotationsformapp

import android.util.Log

object EmailAddressRule : StatelessValidationRule<String, EmailAddress> {

    private const val emailPattern = "[a-zA-Z0-9+._%\\-]{1,256}@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"

    /**
     * Validates the field using the options passed to the [EmailAddress] annotation
     *
     * @param value field value
     * @param options [EmailAddress] object annotated to the field
     */
    override fun validate(value: String, options: EmailAddress): Any {
        Log.e("VALIDATE:", "VALIDATE")
        if (value.matches(emailPattern.toRegex())) {
            Log.e("SUCCESS:", "SUCCESS")
            return "Success"
        } else if (value.isEmpty()) {
            Log.e("EMPTY:", "Empty")
            return "Empty"
        } else {
            Log.e("FAILED:", "Failed")
            return "Failed"
        }
//    override fun validate(value: String, options: EmailAddress): FieldResult {
//        return if (value.matches(emailPattern.toRegex())) {
//            FieldResult.Success
//        } else if (value.isEmpty()) {
//            FieldResult.NoInput
//        } else {
//            FieldResult.Error("Invalid Format", this)
//        }
    }

}