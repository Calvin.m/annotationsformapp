package com.example.annotationsformapp

import android.util.Log
import java.lang.reflect.Field

const val emailPattern = "[a-zA-Z0-9+._%\\-]{1,256}@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"

object Validation {
    fun validateTimestamp(fields: Array<Field>, emailAddress: String) {
/*        Log.e("Validation fields:", "$fields")
        Log.e("Validation emailAddress:", "$emailAddress")*/
        fields.forEach { field ->
/*
            Log.e("Field: ", "${field}")
            Log.e("Email Address: ", "${FormScreenData::emailAddress}")
            Log.e("Email Annotations:", "${field.annotations}")
            Log.e("Email Address: ", "${field.isAnnotationPresent(EmailAddress::class.java)}")
*/
            if (field.isAnnotationPresent(EmailAddress::class.java)) {
                val emailSyntax = field.getAnnotation(EmailAddress::class.java)?.emailAddress
                /*
                Log.e("emailSyntax:", "$emailSyntax")
                */
                if (emailSyntax != null) {
                    if (emailSyntax.toRegex().matches(emailAddress)) {
                        Log.e("CORRECT:", "Email is the correct format: $emailAddress")
                    } else {
                        Log.e("INCORRECT:", "Email is the incorrect format: $emailAddress")
                    }
                }
            }
        }
    }
}