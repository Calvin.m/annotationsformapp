package com.example.annotationsformapp

internal class EmailValidator<V : Any?, A : Annotation>(
    private val validationRule: StatelessValidationRule<V, A>,
    private val options: A
) : FieldValidator<V> {
    /**
     * Validates the field using just it's value, since the option is known
     *
     * @param input value of type [V]
     * @return
     */
    override fun validate(input: V) {
//        return validationRule.validate(input, options)
        validationRule.validate(input, options)
    }
}