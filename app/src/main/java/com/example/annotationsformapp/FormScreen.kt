package com.example.annotationsformapp

import android.util.Log
import android.widget.EditText
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

data class FormScreenData(
    @EmailAddress
    var emailAddress: String
) {
    init {
//        Validation.validateTimestamp(this::class.java.declaredFields, timestamp)
        Log.e("INIT:", "## INIT ##")
        Log.e("this::class.java.declaredFields:", "${this::class.java.declaredFields}")
        Validation.validateTimestamp(this::class.java.declaredFields, emailAddress)
    }
}

@Composable
fun FormScreen() {
//    val emailState = remember { mutableStateOf(FormScreenData().emailAddress) }
    val formData = remember { mutableStateOf(FormScreenData("")) }

    Column(Modifier.padding(12.dp), horizontalAlignment = Alignment.CenterHorizontally) {
        TextField(
            label = { Text("E-mail") },
//            value = FormScreenData().emailAddress,
            value = formData.value.emailAddress,
            onValueChange = { newValue ->
                Log.e("Key Hit:", "$newValue")
                // update the email state value when the user types
                // update the FormScreenData object with the new email value
//                val updatedFormData = FormScreenData(emailState.value)
                // set the TextField value to the updated email value
//                emailState.value = newValue
                formData.value = FormScreenData(newValue)
            })
    }
}
