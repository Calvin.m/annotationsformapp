package com.example.annotationsformapp

internal interface FieldValidator<V: Any?> {
    fun validate(input: V)
}