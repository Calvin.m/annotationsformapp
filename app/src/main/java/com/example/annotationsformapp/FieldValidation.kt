package com.example.annotationsformapp

import kotlin.reflect.KClass

@Target(AnnotationTarget.ANNOTATION_CLASS)
@Retention(AnnotationRetention.RUNTIME)
//@MustBeDocumented
annotation class FieldValidation(
    val fieldType: KClass<out Any>,
    val validator: KClass<out ValidationRule>
)

