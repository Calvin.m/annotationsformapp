package com.example.annotationsformapp

interface StatelessValidationRule<T : Any?, A : Annotation> : ValidationRule {
    //    fun validate(value: T, options: A): FieldResult
    fun validate(value: T, options: A) : Any
}