package com.example.annotationsformapp

//@MustBeDocumented
@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@FieldValidation(
    fieldType = String::class,
    validator = EmailAddressRule::class
)
annotation class EmailAddress(val emailAddress: String = emailPattern)
